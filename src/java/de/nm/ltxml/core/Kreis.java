package de.nm.ltxml.core;

import java.util.HashSet;

import javax.xml.bind.annotation.XmlAttribute;

import org.apache.commons.csv.CSVRecord;

import de.nm.ltxml.core.bez.KreisAbg;
import de.nm.ltxml.core.csv.CSVParsable;

public class Kreis extends Bezeichnung implements CSVParsable {
	// TODO serialize
	private final HashSet<Abgeordneter> abgeordnetenSet = new HashSet<Abgeordneter>();
	private final HashSet<KreisAbg> kreisAbgSet = new HashSet<KreisAbg>();

	@XmlAttribute
	private String type = "?";

	public Kreis() {
		super();
	}

	public Kreis(final String id) {
		super(id);
	}

	public Kreis(final String id, final String bez) {
		super(id, bez);
	}

	public Kreis(final String id, final String typ, final String bez) {
		super(id, bez);
		setType(typ);
	}

	public void addAbgeordneter(final Abgeordneter abgeordneter) {
		abgeordnetenSet.add(abgeordneter);
	}

	public void addKreisAbg(final KreisAbg kreisAbg) {
		kreisAbgSet.add(kreisAbg);
	}

	public HashSet<Abgeordneter> getSetAbg() {
		return abgeordnetenSet;
	}

	public HashSet<KreisAbg> getSetKreisAbg() {
		return kreisAbgSet;
	}

	public String getType() {
		return type;
	}

	@Override
	public void parseFromLine(final CSVRecord line) {
		id = line.get("id");
		bezeichnung = line.get("Bezeichnung");
		type = line.get("Type");
	}

	public void setType(final String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Kreis: type=" + type + ", id=" + id + ", bezeichnung=" + bezeichnung;
	}

}
