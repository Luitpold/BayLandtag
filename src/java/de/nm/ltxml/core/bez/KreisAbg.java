package de.nm.ltxml.core.bez;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import de.nm.ltxml.core.Abgeordneter;
import de.nm.ltxml.core.Base;
import de.nm.ltxml.core.Kreis;

@XmlAccessorType(XmlAccessType.NONE)
public class KreisAbg extends Base {

	Abgeordneter abg;

	private Date bis;
	private Kreis kreis;

	private Date von;

	public KreisAbg() {
		super();
	}

	public KreisAbg(final String id) {
		super(id);
	}

	public Abgeordneter getAbg() {
		return abg;
	}

	public Date getBis() {
		return bis;
	}

	public Kreis getKreis() {
		return kreis;
	}

	public Date getVon() {
		return von;
	}

	public void setAbg(final Abgeordneter abg) {
		this.abg = abg;
		abg.addKreisAbg(this);
	}

	public void setBis(final Date bis) {
		this.bis = bis;
	}

	public void setKreis(final Kreis kreis) {
		this.kreis = kreis;
		kreis.addAbgeordneter(abg);
		kreis.addKreisAbg(this);
	}

	public void setVon(final Date von) {
		this.von = von;
	}

	@Override
	public String toString() {
		String tmp = "KreisAbg: id=" + id + ", abg=" + abg.toString() + ", kreis=" + kreis.toString();
		tmp += ", von=" + getVon().toString();
		tmp += ", bis=" + getBis().toString();
		return tmp;
	}
}
