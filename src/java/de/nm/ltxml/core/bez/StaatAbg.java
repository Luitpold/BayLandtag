package de.nm.ltxml.core.bez;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import de.nm.ltxml.core.Abgeordneter;
import de.nm.ltxml.core.Bezeichnung;
import de.nm.ltxml.core.Staatsregierung;

@XmlAccessorType(XmlAccessType.NONE)
public class StaatAbg extends Bezeichnung {

	Abgeordneter abg;
	Staatsregierung staRe;
	Date von, bis;

	public StaatAbg() {
		// TODO Auto-generated constructor stub
	}

	public StaatAbg(final String id) {
		super(id);
	}

	public void setAbg(final Abgeordneter abg) {
		this.abg = abg;
		abg.addStAbg(this);
	}

	public void setBis(final Date bis) {
		this.bis = bis;
	}

	public void setStaat(final Staatsregierung staat) {
		staRe = staat;
		staRe.addStAbg(this);
	}

	public void setVon(final Date von) {
		this.von = von;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("StaatAbg: id=").append(id).append(", abg=").append(abg.toString()).append(", partei=")
				.append(staRe.toString()).append(", von=").append(von).append(", bis=").append(bis);

		return builder.toString();
	}

}
