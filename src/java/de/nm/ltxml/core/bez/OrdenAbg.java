package de.nm.ltxml.core.bez;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import de.nm.ltxml.core.Abgeordneter;
import de.nm.ltxml.core.Bezeichnung;
import de.nm.ltxml.core.Orden;

@XmlAccessorType(XmlAccessType.NONE)
public class OrdenAbg extends Bezeichnung {

	Abgeordneter abg;
	Orden orden;

	public OrdenAbg() {
		// TODO Auto-generated constructor stub
	}

	public OrdenAbg(final String id) {
		super(id);
	}

	public void setAbg(final Abgeordneter abg) {
		this.abg = abg;
		this.abg.addOrdenAbg(this);
	}

	public void setOrden(final Orden ord) {
		orden = ord;
		orden.addOrdenAbg(this);
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder("OrdenAbg: id=");
		builder.append(id).append(", abg=").append(abg.toString()).append(", orden=").append(orden.toString());
		return builder.toString();
	}

}
