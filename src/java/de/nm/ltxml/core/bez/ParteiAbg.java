package de.nm.ltxml.core.bez;

import java.util.Date;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlIDREF;

import de.nm.ltxml.core.Abgeordneter;
import de.nm.ltxml.core.Base;
import de.nm.ltxml.core.Partei;

public class ParteiAbg extends Base {

	@XmlAttribute
	@XmlIDREF
	Abgeordneter abg;

	@XmlAttribute
	private Date bis;

	@XmlAttribute
	@XmlIDREF
	Partei partei;

	@XmlAttribute
	private Date von;

	public ParteiAbg() {
		super();
	}

	public ParteiAbg(final String id) {
		super(id);
	}

	public Abgeordneter getAbg() {
		return abg;
	}

	public Date getBis() {
		return bis;
	}

	public Partei getPartei() {
		return partei;
	}

	public Date getVon() {
		return von;
	}

	public void setAbg(final Abgeordneter abg) {
		this.abg = abg;
		abg.add(this);
	}

	public void setBis(final Date bis) {
		this.bis = bis;
	}

	public void setPartei(final Partei partei) {
		this.partei = partei;
		partei.setParteiAbg(this);
	}

	public void setVon(final Date von) {
		this.von = von;
	}

	@Override
	public String toString() {
		return "ParteiAbg: id=" + id + ", abg=" + abg + ", partei=" + partei + ", von=" + von + ", bis=" + bis;
	}

}
