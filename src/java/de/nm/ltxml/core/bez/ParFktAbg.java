package de.nm.ltxml.core.bez;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlIDREF;

import org.apache.commons.csv.CSVRecord;

import de.nm.ltxml.core.Abgeordneter;
import de.nm.ltxml.core.BayLandtag;
import de.nm.ltxml.core.Bezeichnung;
import de.nm.ltxml.core.ParFkt;
import de.nm.ltxml.core.csv.CSVParsable;

@XmlAccessorType(XmlAccessType.NONE)
public class ParFktAbg extends Bezeichnung implements CSVParsable {

	@XmlIDREF
	Abgeordneter abg;
	@XmlIDREF
	ParFkt parFkt;
	@XmlAttribute
	String type;
	@XmlAttribute
	Date von, bis;

	public ParFktAbg() {
		// TODO Auto-generated constructor stub
	}

	public ParFktAbg(final String id) {
		super(id);
	}

	public Date getBis() {
		return bis;
	}

	public Date getVon() {
		return von;
	}

	@Override
	public void parseFromLine(final CSVRecord line) {
		abg = (Abgeordneter) BayLandtag.getInstance().getById(line.get("a_id"), Abgeordneter.class);
		parFkt = (ParFkt) BayLandtag.getInstance().getById(line.get("pa_id"), ParFkt.class);
		type = line.get("Typ");
		final SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd", Locale.GERMANY);
		try {
			von = format.parse(line.get("von"));
		} catch (final ParseException e1) {
			e1.printStackTrace();
		}
		try {
			bis = format.parse(line.get("bis"));
		} catch (final ParseException e) {
			e.printStackTrace();
		}

	}

	public void setAbg(final Abgeordneter abg) {
		this.abg = abg;
		abg.addParFktAbg(this);
	}

	public void setBis(final Date bis) {
		this.bis = bis;
	}

	public void setParfkt(final ParFkt parfkt) {
		this.parFkt = parfkt;
		parFkt.addParFktAbg(this);
	}

	public void setVon(final Date von) {
		this.von = von;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("ParFktAbg: id=").append(id).append(", abg=").append(abg.toString()).append(", type=")
				.append(type).append(", parfkt=").append(parFkt.toString()).append(", von=").append(von.toString())
				.append(", bis=").append(bis.toString());
		return builder.toString();
	}
}
