package de.nm.ltxml.core;

import java.util.HashSet;

import org.apache.commons.csv.CSVRecord;

import de.nm.ltxml.core.bez.OrdenAbg;
import de.nm.ltxml.core.csv.CSVParsable;

public class Orden extends Bezeichnung implements CSVParsable {

	HashSet<OrdenAbg> setOrdenAbg = new HashSet<OrdenAbg>();

	public Orden() {
		super();
	}

	public Orden(final String id) {
		super(id);
	}

	public Orden(final String id, final String bez) {
		super(id, bez);
	}

	public void addOrdenAbg(final OrdenAbg o) {
		setOrdenAbg.add(o);
	}

	public HashSet<OrdenAbg> getSetOrdenAbg() {
		return setOrdenAbg;
	}

	@Override
	public void parseFromLine(final CSVRecord line) {
		id = line.get("id");
		bezeichnung = line.get("Bezeichnung");
	}

	@Override
	public String toString() {
		return "Orden: id=" + id + ", bezeichnung=" + bezeichnung;
	}

}
