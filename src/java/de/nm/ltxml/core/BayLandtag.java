package de.nm.ltxml.core;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import de.nm.ltxml.core.bez.KreisAbg;
import de.nm.ltxml.core.bez.OrdenAbg;
import de.nm.ltxml.core.bez.ParFktAbg;
import de.nm.ltxml.core.bez.ParteiAbg;
import de.nm.ltxml.core.bez.StaatAbg;

@XmlRootElement(name = "baylandtag")
@XmlAccessorType(XmlAccessType.FIELD)
public class BayLandtag {

	@XmlTransient
	private static BayLandtag instance;

	public static BayLandtag getInstance() {
		return instance;
	}

	@XmlElementWrapper(name = "abgeordnete")
	@XmlElement(name = "abg")
	List<Abgeordneter> abgeordnetenListe = new ArrayList<>();

	@XmlElementWrapper(name = "familie")
	@XmlElement(name = "fam")
	List<Familienstand> familienstandListe = new ArrayList<>();

	@XmlElementWrapper(name = "konfession")
	@XmlElement(name = "konf")
	List<Konfession> konfessionsListe = new ArrayList<>();

	@XmlElementWrapper(name = "kreisabg")
	@XmlElement(name = "krbg")
	List<KreisAbg> kreisAbgListe = new ArrayList<>();

	@XmlElementWrapper(name = "kreis")
	@XmlElement(name = "kr")
	List<Kreis> kreisListe = new ArrayList<>();

	List<OrdenAbg> ordenAbgListe = new ArrayList<>();
	@XmlElementWrapper(name = "orden")
	@XmlElement(name = "ord")
	List<Orden> ordensListe = new ArrayList<>();

	@XmlElementWrapper(name = "parfktabg")
	@XmlElement(name = "pfagb")
	List<ParFktAbg> parFktAbgListe = new ArrayList<>();

	@XmlElementWrapper(name = "parteiabg")
	@XmlElement(name = "pagb")
	List<ParteiAbg> parteiabgeordnetenliste = new ArrayList<>();

	@XmlElementWrapper(name = "parfkt")
	@XmlElement(name = "pf")
	List<ParFkt> parteienFunktionsListe = new ArrayList<>();

	@XmlElementWrapper(name = "partei")
	@XmlElement(name = "pa")
	List<Partei> parteienListe = new ArrayList<>();
	List<StaatAbg> staatsAbgListe = new ArrayList<>();
	@XmlElementWrapper(name = "staatsregierung")
	@XmlElement(name = "staat")
	List<Staatsregierung> staatsregierungsListe = new ArrayList<>();

	public BayLandtag() {
		super();
		instance = this;
	}

	public void add(final Abgeordneter abgeordneter) {
		abgeordnetenListe.add(abgeordneter);
	}

	public void add(final Familienstand familienstand) {
		familienstandListe.add(familienstand);
	}

	public void add(final Konfession konf) {
		konfessionsListe.add(konf);
	}

	public void add(final Kreis kr) {
		kreisListe.add(kr);
	}

	public void add(final KreisAbg krbg) {
		kreisAbgListe.add(krbg);
	}

	public void add(final Orden ord) {
		ordensListe.add(ord);
	}

	public void add(final OrdenAbg orbg) {
		ordenAbgListe.add(orbg);
	}

	public void add(final ParFkt pf) {
		parteienFunktionsListe.add(pf);
	}

	public void add(final ParFktAbg pfbg) {
		parFktAbgListe.add(pfbg);
	}

	public void add(final Partei pa) {
		parteienListe.add(pa);
	}

	public void add(final ParteiAbg pagb) {
		parteiabgeordnetenliste.add(pagb);
	}

	public void add(final StaatAbg stbg) {
		staatsAbgListe.add(stbg);
	}

	public void add(final Staatsregierung staat) {
		staatsregierungsListe.add(staat);
	}

	public Abgeordneter getAbgeordneter(final String vorname, final String nachname, final String dateString,
			final String gebIn) {
		final SimpleDateFormat format = new SimpleDateFormat("dd.mm.yyyy", Locale.GERMANY);
		final SimpleDateFormat format2 = new SimpleDateFormat("yyyy-mm-dd", Locale.GERMANY);
		for (final Abgeordneter tmp : abgeordnetenListe) {
			if (dateString.length() == 0 || dateString.equals("null") || tmp.gebAm == null) {
				try {
					if (tmp.vorname.equals(vorname) && tmp.name.equals(nachname) && tmp.gebIn.equals(gebIn))
						return tmp;
				} catch (final Exception e) {
					e.printStackTrace();
					return null;
				}
			} else if (dateString.length() == 4) {
				try {
					if (tmp.vorname.equals(vorname) && tmp.name.equals(nachname) && tmp.gebIn.equals(gebIn)
							&& tmp.gebAm.getYear() == Integer.parseInt(dateString))
						return tmp;
				} catch (final Exception e) {
					e.printStackTrace();
					return null;
				}
			} else {
				if (dateString.contains("-")) {
					try {
						if (tmp.vorname.equals(vorname) && tmp.name.equals(nachname) && tmp.gebIn.equals(gebIn)
								&& tmp.gebAm.compareTo(format2.parse(dateString)) == 0)
							return tmp;
					} catch (final Exception e) {
						e.printStackTrace();
						return null;
					}
				}
				try {
					if (tmp.vorname.equals(vorname) && tmp.name.equals(nachname) && tmp.gebIn.equals(gebIn)
							&& tmp.gebAm.compareTo(format.parse(dateString)) == 0)
						return tmp;
				} catch (final Exception e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;
	}

	public Object getById(final String id, final Class type) {
		if (type.equals(Familienstand.class)) {
			for (final Familienstand f : familienstandListe) {
				if (f.id.equals(id))
					return f;
			}
		} else if (type.equals(Konfession.class)) {
			for (final Konfession k : konfessionsListe) {
				if (k.id.equals(id))
					return k;
			}
		} else if (type.equals(Kreis.class)) {
			for (final Kreis k : kreisListe) {
				if (k.id.equals(id))
					return k;
			}
		} else if (type.equals(Abgeordneter.class)) {
			for (final Abgeordneter abg : abgeordnetenListe) {
				if (abg.id.equals(id))
					return abg;
			}
		} else if (type.equals(Orden.class)) {
			for (final Orden o : ordensListe) {
				if (o.id.equals(id))
					return o;
			}
		} else if (type.equals(ParFkt.class)) {
			for (final ParFkt p : parteienFunktionsListe) {
				if (p.id.equals(id))
					return p;
			}
		} else if (type.equals(Partei.class)) {
			for (final Partei p : parteienListe) {
				if (p.id.equals(id))
					return p;
			}
		} else if (type.equals(Staatsregierung.class)) {
			for (final Staatsregierung s : staatsregierungsListe) {
				if (s.id.equals(id))
					return s;
			}
		}
		return null;
	}

	Date parseDate(final String dateString) {
		// Will parse dd.mm.yyyy
		try {
			final String[] components = dateString.split(".");
			final Date tmp = new Date();
			tmp.setYear(Integer.valueOf(components[2]));
			tmp.setMonth(Integer.valueOf(components[1]));
			tmp.setDate(Integer.valueOf(components[0]));

			return tmp;
		} catch (final Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
