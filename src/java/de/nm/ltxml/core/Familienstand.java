package de.nm.ltxml.core;

import org.apache.commons.csv.CSVRecord;

import de.nm.ltxml.core.csv.CSVParsable;

public class Familienstand extends Bezeichnung implements CSVParsable {

	public Familienstand() {
		super();
	}

	public Familienstand(final String id, final String bezeichnung) {
		super(id, bezeichnung);
	}

	@Override
	public void parseFromLine(final CSVRecord line) {
		id = line.get("id");
		bezeichnung = line.get("Bezeichnung");
	}

	@Override
	public String toString() {
		return "Familienstand: id=" + id + ", bezeichnung=" + bezeichnung;
	}

}
