package de.nm.ltxml.core.csv;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import de.nm.ltxml.core.Abgeordneter;
import de.nm.ltxml.core.BayLandtag;
import de.nm.ltxml.core.Familienstand;
import de.nm.ltxml.core.Konfession;
import de.nm.ltxml.core.Kreis;
import de.nm.ltxml.core.Orden;
import de.nm.ltxml.core.ParFkt;
import de.nm.ltxml.core.Partei;
import de.nm.ltxml.core.Staatsregierung;
import de.nm.ltxml.core.bez.ParFktAbg;

public class BayLandtagGenerator {

	public static void main(final String[] args) {
		new BayLandtagGenerator();
	}

	BayLandtag bayLandtag;

	CSVParser parser;

	public BayLandtagGenerator() {
		bayLandtag = new BayLandtag();

		ladeKonfession();
		ladeFamilienstand();
		ladeAbgeordnete();
		ladeBilder();
		ladeKreis();
		ladeOrden();
		ladeParfkt();
		ladePartei();
		ladeStaatsregierung();
		ladeParFktAbg();
		exportXML();
	}

	void exportXML() {
		JAXBContext context;
		try {
			context = JAXBContext.newInstance(BayLandtag.class);
			final Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			final File output = new File("target/csv_to_xml_test.xml");
			m.marshal(bayLandtag, output);
		} catch (final JAXBException e) {
			e.printStackTrace();
		}
	}

	void ladeAbgeordnete() {
		final Iterator<CSVRecord> iterator = setupParser(new File("src/csv/abgeordneter.csv"));
		while (iterator.hasNext()) {
			final Abgeordneter tmp = new Abgeordneter();
			tmp.parseFromLine(iterator.next());
			bayLandtag.add(tmp);
		}
	}

	void ladeBilder() {
		final Iterator<CSVRecord> iterator = setupParser(new File("src/csv/bild.csv"));
		while (iterator.hasNext()) {
			final CSVRecord line = iterator.next();
			final Abgeordneter abg = bayLandtag.getAbgeordneter(line.get("Vorname"), line.get("Name"),
					line.get("geb_am"), line.get("geb_in"));
			if (abg != null)
				abg.setBild(loadBytes(line.get("bild")));

		}
	}

	void ladeFamilienstand() {
		final Iterator<CSVRecord> iterator = setupParser(new File("src/csv/familienstand.csv"));
		while (iterator.hasNext()) {
			final Familienstand tmp = new Familienstand();
			tmp.parseFromLine(iterator.next());
			bayLandtag.add(tmp);
		}
	}

	void ladeKonfession() {
		final Iterator<CSVRecord> iterator = setupParser(new File("src/csv/konfession.csv"));
		while (iterator.hasNext()) {
			final Konfession tmp = new Konfession();
			tmp.parseFromLine(iterator.next());
			bayLandtag.add(tmp);
		}
	}

	void ladeKreis() {
		final Iterator<CSVRecord> iterator = setupParser(new File("src/csv/kreis.csv"));
		while (iterator.hasNext()) {
			final Kreis tmp = new Kreis();
			tmp.parseFromLine(iterator.next());
			bayLandtag.add(tmp);
		}
	}

	void ladeOrden() {
		final Iterator<CSVRecord> iterator = setupParser(new File("src/csv/orden.csv"));
		while (iterator.hasNext()) {
			final Orden tmp = new Orden();
			tmp.parseFromLine(iterator.next());
			bayLandtag.add(tmp);
		}
	}

	void ladeParfkt() {
		final Iterator<CSVRecord> iterator = setupParser(new File("src/csv/parfkt.csv"));
		while (iterator.hasNext()) {
			final ParFkt tmp = new ParFkt();
			tmp.parseFromLine(iterator.next());
			bayLandtag.add(tmp);
		}
	}

	void ladeParFktAbg() {
		final Iterator<CSVRecord> iterator = setupParser(new File("src/csv/zt_fkt_abg.csv"));
		while (iterator.hasNext()) {
			final ParFktAbg tmp = new ParFktAbg();
			tmp.parseFromLine(iterator.next());
			bayLandtag.add(tmp);
		}
	}

	void ladePartei() {
		final Iterator<CSVRecord> iterator = setupParser(new File("src/csv/partei.csv"));
		while (iterator.hasNext()) {
			final Partei tmp = new Partei();
			tmp.parseFromLine(iterator.next());
			bayLandtag.add(tmp);
		}
	}

	void ladeStaatsregierung() {
		final Iterator<CSVRecord> iterator = setupParser(new File("src/csv/staatsregierung.csv"));
		while (iterator.hasNext()) {
			final Staatsregierung tmp = new Staatsregierung();
			tmp.parseFromLine(iterator.next());
			bayLandtag.add(tmp);
		}
	}

	byte[] loadBytes(final String filename) {
		if (filename.endsWith("null")) {
			return null;
		}
		try {
			return Files.readAllBytes(Paths.get("src/images/" + filename));
		} catch (final IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	Iterator<CSVRecord> setupParser(final File file) {
		try {
			parser = CSVParser.parse(file, StandardCharsets.UTF_8, CSVFormat.EXCEL.withHeader());
			return parser.iterator();
		} catch (final IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
