package de.nm.ltxml.core.csv;

import org.apache.commons.csv.CSVRecord;

public interface CSVParsable {
	public void parseFromLine(CSVRecord line);
}
