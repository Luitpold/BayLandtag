package de.nm.ltxml.core;

import java.util.HashSet;

import org.apache.commons.csv.CSVRecord;

import de.nm.ltxml.core.bez.ParFktAbg;
import de.nm.ltxml.core.csv.CSVParsable;

public class ParFkt extends Bezeichnung implements CSVParsable {

	HashSet<ParFktAbg> parFktAbg = new HashSet<ParFktAbg>();

	public ParFkt() {
		super();
	}

	public ParFkt(final String id) {
		super(id);
	}

	public ParFkt(final String id, final String bez) {
		super(id, bez);
	}

	public void addParFktAbg(final ParFktAbg parFktAbg2) {
		parFktAbg.add(parFktAbg2);
	}

	public HashSet<ParFktAbg> getSetParFktAbg() {
		// TODO Auto-generated method stub
		return parFktAbg;
	}

	@Override
	public void parseFromLine(final CSVRecord line) {
		id = line.get("id");
		bezeichnung = line.get("Bezeichnung");
	}

	@Override
	public String toString() {
		final String tmp = "ParFkt: id=" + id + ", bezeichnung=" + bezeichnung;
		final StringBuilder builder = new StringBuilder(tmp);
		return builder.toString();
	}

}
