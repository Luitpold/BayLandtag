package de.nm.ltxml.core;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlIDREF;

import org.apache.commons.csv.CSVRecord;

import de.nm.ltxml.core.bez.KreisAbg;
import de.nm.ltxml.core.bez.OrdenAbg;
import de.nm.ltxml.core.bez.ParFktAbg;
import de.nm.ltxml.core.bez.ParteiAbg;
import de.nm.ltxml.core.bez.StaatAbg;
import de.nm.ltxml.core.csv.CSVParsable;

public class Abgeordneter extends Base implements CSVParsable {

	@XmlAttribute
	String beruf = "??";

	@XmlElement(name = "bild")
	byte[] bilder;

	@XmlIDREF
	@XmlAttribute
	private Familienstand fam;

	@XmlAttribute
	Date gebAm;

	@XmlAttribute
	String gebIn = "??";

	@XmlAttribute
	Date gesAm;

	@XmlAttribute
	String gesIn = "??";

	@XmlIDREF
	@XmlAttribute
	private Konfession konf;

	@XmlAttribute
	String name = "??";
	@XmlElementWrapper(name = "kreisabg")
	@XmlElement(name = "kagb")
	@XmlIDREF
	Set<KreisAbg> setKreisAbg = new HashSet<KreisAbg>();

	HashSet<OrdenAbg> setOrdenAbg = new HashSet<OrdenAbg>();
	HashSet<ParFktAbg> setParFktAbg = new HashSet<ParFktAbg>();
	@XmlElementWrapper(name = "parteiabg")
	@XmlElement(name = "pagb")
	@XmlIDREF
	Set<ParteiAbg> setParteiAbg = new HashSet<ParteiAbg>();

	HashSet<StaatAbg> setStaatAbg = new HashSet<StaatAbg>();

	@XmlAttribute
	String titel = "??";

	@XmlAttribute
	String vorname = "??";

	public Abgeordneter() {
		super();
	}

	public Abgeordneter(final String id, final String name, final String vorname) {
		super(id);
		setName(name);
		setVorname(vorname);
	}

	public boolean add(final ParteiAbg abg) {
		return setParteiAbg.add(abg);
	}

	public void addKreisAbg(final KreisAbg kreisAbg) {
		setKreisAbg.add(kreisAbg);
	}

	public void addOrdenAbg(final OrdenAbg ordenAbg) {
		setOrdenAbg.add(ordenAbg);
	}

	public void addParFktAbg(final ParFktAbg a) {
		setParFktAbg.add(a);
	}

	public void addStAbg(final StaatAbg staatAbg) {
		setStaatAbg.add(staatAbg);
	}

	public String getBeruf() {
		return beruf;
	}

	public byte[] getBild() {
		return bilder;
	}

	public Familienstand getFam() {
		return fam;
	}

	public Date getGebAm() {
		return gebAm;
	}

	public String getGebIn() {
		return gebIn;
	}

	public Date getGesAm() {
		return gesAm;
	}

	public String getGesIn() {
		return gesIn;
	}

	public Konfession getKonf() {
		return konf;
	}

	public String getName() {
		return name;
	}

	public Set<KreisAbg> getSetKreisAbg() {
		return setKreisAbg;
	}

	public HashSet<OrdenAbg> getSetOrdenAbg() {
		return setOrdenAbg;
	}

	public HashSet<ParFktAbg> getSetParFktAbg() {
		// TODO Auto-generated method stub
		return setParFktAbg;
	}

	public Set<ParteiAbg> getSetParteiAbg() {
		return setParteiAbg;
	}

	public HashSet<StaatAbg> getSetStaatAbg() {
		return setStaatAbg;
	}

	public String getTitel() {
		return titel;
	}

	public String getVorname() {
		return vorname;
	}

	Date parseDate(final String dateString) {
		// Will parse yyyy-mm-dd
		try {
			final String[] components = dateString.split("-");
			final Date tmp = new Date();
			tmp.setYear(Integer.valueOf(components[0]));
			tmp.setMonth(Integer.valueOf(components[1]));
			tmp.setDate(Integer.valueOf(components[2]));

			return tmp;
		} catch (final Exception e) {
			e.printStackTrace();
			System.out.println("Could not parse: " + dateString);
		}
		return null;
	}

	@Override
	public void parseFromLine(final CSVRecord line) {
		id = line.get("id");
		name = line.get("Name");
		vorname = line.get("Vorname");
		titel = line.get("Titel");
		beruf = line.get("Beruf");
		final SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd", Locale.GERMANY);
		final SimpleDateFormat format2 = new SimpleDateFormat("dd.mm.yyyy", Locale.GERMANY);
		// gebAm = parseDate(line.get("geb_am"));
		// gesAm = parseDate(line.get("ges_am"));

		try {
			final String tmp = line.get("geb_am");
			if (!tmp.equals("null") && tmp.length() > 0 && tmp != null) {
				if (tmp.contains("-")) {
					gebAm = format.parse(tmp);
				} else {
					gebAm = format2.parse(tmp);
				}
			}
		} catch (final ParseException e1) {
			e1.printStackTrace();
		}
		try {
			final String tmp = line.get("ges_am");
			if (!tmp.equals("null") && tmp.length() > 0 && tmp != null) {
				if (tmp.contains("-")) {
					gesAm = format.parse(tmp);
				} else {
					gesAm = format.parse(tmp);
				}
			}
		} catch (final ParseException e) {
			e.printStackTrace();
		}

		gebIn = line.get("geb_in");
		gesIn = line.get("ges_in");

		konf = (Konfession) BayLandtag.getInstance().getById(line.get("k_id"), Konfession.class);
		fam = (Familienstand) BayLandtag.getInstance().getById(line.get("f_id"), Familienstand.class);
	}

	public void setBeruf(final String beruf) {
		this.beruf = beruf;
	}

	public void setBild(final byte[] bilder) {
		this.bilder = bilder;
	}

	public void setFam(final Familienstand fam) {
		this.fam = fam;
	}

	public void setGebam(final Date d) {
		gebAm = d;
	}

	public void setGebAm(final Date gebAm) {
		this.gebAm = gebAm;
	}

	public void setGebin(final String string) {
		gebIn = string;
	}

	public void setGebIn(final String gebIn) {
		this.gebIn = gebIn;
	}

	public void setGesam(final Date d) {
		gesAm = d;
	}

	public void setGesAm(final Date gesAm) {
		this.gesAm = gesAm;
	}

	public void setGesin(final String string) {
		gesIn = string;
	}

	public void setGesIn(final String gesIn) {
		this.gesIn = gesIn;
	}

	public void setKonf(final Konfession konf) {
		this.konf = konf;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public void setSetKreisAbg(final Set<KreisAbg> setKreisAbg) {
		this.setKreisAbg = setKreisAbg;
	}

	public void setSetParteiAbg(final Set<ParteiAbg> setParteiAbg) {
		this.setParteiAbg = setParteiAbg;
	}

	public void setTitel(final String titel) {
		this.titel = titel;
	}

	public void setVorname(final String vorname) {
		this.vorname = vorname;
	}

	@Override
	public String toString() {
		return "Abgeordneter: id=" + id + ", name=" + name + ", vorname=" + vorname + ", titel=" + titel + ", beruf="
				+ beruf + ", gebAm=" + gebAm + ", gebIn=" + gebIn + ", gesAm=" + gesAm + ", gesIn=" + gesIn + ", konf="
				+ konf + ", fam=" + fam;
	}
}
