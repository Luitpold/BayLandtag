package de.nm.ltxml.core;

import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlIDREF;

import org.apache.commons.csv.CSVRecord;

import de.nm.ltxml.core.bez.ParteiAbg;
import de.nm.ltxml.core.csv.CSVParsable;

public class Partei extends Bezeichnung implements CSVParsable {

	@XmlAttribute
	private String name = "??";

	@XmlElementWrapper(name = "parteiabg")
	@XmlElement(name = "pabg")
	@XmlIDREF
	Set<ParteiAbg> parteiAbg = new HashSet<ParteiAbg>();

	public Partei() {
		super();
	}

	public Partei(final String id) {
		super(id);
	}

	public Partei(final String id, final String bez) {
		super(id, bez);
	}

	public Partei(final String id, final String nam, final String bez) {
		super(id, bez);
		setName(nam);
	}

	public String getName() {
		return name;
	}

	public Set<ParteiAbg> getSetParteiAbg() {
		return parteiAbg;
	}

	@Override
	public void parseFromLine(final CSVRecord line) {
		id = line.get("id");
		bezeichnung = line.get("Bezeichnung");
		name = line.get("Name");
	}

	public void setName(final String name) {
		this.name = name;
	}

	public void setParteiAbg(final ParteiAbg partei) {
		parteiAbg.add(partei);
	}

	@Override
	public String toString() {
		return "Partei: name=" + name + ", id=" + id + ", bezeichnung=" + bezeichnung;
	}

}
