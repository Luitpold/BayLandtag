package de.nm.ltxml.core;

import java.util.HashSet;

import org.apache.commons.csv.CSVRecord;

import de.nm.ltxml.core.bez.StaatAbg;
import de.nm.ltxml.core.csv.CSVParsable;

public class Staatsregierung extends Bezeichnung implements CSVParsable {

	HashSet<StaatAbg> setStaatAbg = new HashSet<StaatAbg>();

	public Staatsregierung() {
		super();
	}

	public Staatsregierung(final String id) {
		super(id);
	}

	public Staatsregierung(final String id, final String bez) {
		super(id, bez);
	}

	public void addStAbg(final StaatAbg staatAbg) {
		setStaatAbg.add(staatAbg);
	}

	public HashSet<StaatAbg> getSetStaatAbg() {
		return setStaatAbg;
	}

	@Override
	public void parseFromLine(final CSVRecord line) {
		id = line.get("id");
		bezeichnung = line.get("Bezeichnung");
	}

	@Override
	public String toString() {
		return "Staatsregierung: id=" + id + ", bezeichnung=" + bezeichnung;
	}

}
